# Computing Finance Application - CSUI 2019/2020

## Group

Group Name: Tim 7

- Muhammad Yudistira Hanifmuti - 1606829560
- Muhammad Fakhrillah Abdul Azis - 1606917531
- Agas Yanpratama - 1606918396
- Kevin Raikhan Zain - 1706075041
- Stefanus Khrisna A. H. - 1706074921


## Deployment:

* frontend: http://tabungan-cuan.herokuapp.com/
* backend: http://backend-seven.herokuapp.com/

How to access deployment:
1. Open Backend to wake up the heroku. Wait until `Happstack` Text displayed.
2. You can now open frontend to access the webpage.
3. You can use sidebar to fill form and access feature `Dana Impian` to calculate mutual funds deposit each month to reach certain money target.
4. Sidebar can invoke graph that will calculate money after some time using mutual funds each month without deposit.

## Prsentation:
* [Google Docs Presentation - Tim 7](https://docs.google.com/presentation/d/1iG2HQsPfmj3mdtCNk8hZW0msFAhX18pPNDl7CpuNLZk/edit?usp=sharing)


* * *

## Setup
How to run the project locally

### Running Backend locally
In order to run Backend Haskell locally, you need to:

1. Clone the repo
2. Navigate to the project's folder on Terminal/Cmd
3. Install [Happstack](http://happstack.com/page/view-page-slug/2/download):
4. Set environment variable port, In linux you can run `export PORT=8000`
5. Change directory to `src` and run the project:
  `runhaskell Server.hs`
6. Open a browser on http://localhost:8000

### Running Frontend Locally
1. Change directory to frontend: `cd frontend`
2. Run `yarn install` to install all dependencies
3. run `yarn dev` to start frontend 
4. Open a browser on http://localhost:3000

**Please use `yarn` as package manager, you can use `nvm` to change node version and use node 13.2.0 with command `nvm use 13.2.0`** 

* * * 

# System Documetation

## Backend

All API on Backend that deployed on http://backend-seven.herokuapp.com/ and every API usage:

### Tabungan-Cuan API

#### Usage: To Calculate someone money after save the money in form of mutual fund for X Month

#### How to Call:
`http://backend-seven.herokuapp.com/monthly-saving?start=x&bulan=y&persen=z`

* x is starting money
* y is total month someone want to save his/her money.
* z is percentage of mutual fund profit each year.

#### API Return:

* Example:

```
Input: http://backend-seven.herokuapp.com/monthly-saving?start=100&bulan=12&persen=10
Output: [100.0,100.83333333333333,101.6736111111111,102.52089120370368,103.37523196373455,104.23669223009901,105.10533133201649,105.98120909311663,106.86438583555926,107.75492238418893,108.65288007072382,109.55832073797987]
```

* Explanation: Input will call api to search total money increase with starting money 100 in twelve month and mutual fund profit percentage is 10% each year. Output will return array of money increase each month for 12 mont.


---
#### Usage: To Calculate how much money needed every month to reach the target

#### How to Call:
`http://backend-seven.herokuapp.com/monthly-deposit?awal=x&total=y&targetbulan=z`

* x is starting money
* y is target money
* z is total month someone want to save his/her money.

#### API Return:

* Example:

```
Input: http://backend-seven.herokuapp.com/monthly-deposit?awal=10000&total=200000&targetbulan=6
Output: 37572
```

---
#### Usage: To Calculate how much money needed every month to reach the target with user defined profit

#### How to Call:
`http://backend-seven.herokuapp.com/monthly-deposit-profit?awal=x&total=y&targetbulan=z&profit=w`

* x is starting money
* y is target money
* z is total month someone want to save his/her money.
* w is profit value.


#### API Return:

* Example:

```
Input: http://backend-seven.herokuapp.com/monthly-deposit-profit?awal=10000&total=200000&targetbulan=6&profit=0.005
Output: 37572
```

---
#### Usage: To Calculate a Prediction of Monthly NAV (Net Asset Value) per Unit Price

#### How to Call:
`http://backend-seven.herokuapp.com/monthly-nab?counter=x`

* x is the total of month that you want to know about the price

#### API Return:


* Example:

```
Input: http://backend-seven.herokuapp.com/monthly-nab?counter=12
Output: [1058.271111111111,1063.7406089703702,1069.238375017732,1074.7645553526156,1080.3192968295298,1085.9027470619772,1091.5150544263759,1097.1563680660029,1102.8268378949574,1108.5266146021447,1114.2558496552801,1120.0146953049152]

```

* Explanation: The X is total of month which is 12 months. Input will call api to count the price of NAV within range of x (total of month). The output will return an array of the price of NAV for 12 months.



***

### Reference:
* https://github.com/hpbl/HaskellKoans
* https://medium.com/better-programming/how-to-deploy-your-react-app-to-heroku-aedc28b218ae
