import React, {Props, useState, useEffect} from "react";

type NABProps = {
    priceNAB: number[];
};

const MonthlyNAB = ({priceNAB}: NABProps) => {

    const [search, setSearch] = React.useState("");
    const [query, setQuery] = React.useState("");
    const [result, loading] = useAsyncHook(query);
    let [toString, setToString] = useState("");

    function useAsyncHook(bulan: string) {
        const [result, setResult] = React.useState([]);
        const [loading, setLoading] = React.useState("false");

        React.useEffect(() => {
            async function fetchBookList() {
                try {
                    setLoading("true");
                    const response = await fetch(
                        `http://backend-seven.herokuapp.com/monthly-nab?counter=${bulan}`
                    );

                    const json = await response.json();
                    // console.log(json);
                    setResult(json);


                    toString = "";
                    let resultString = toString += (json);
                    setToString(resultString);

                } catch (error) {
                    setLoading("null");
                }
            }

            if (bulan !== "") {
                fetchBookList();
            }
        }, [bulan]);

        return [result, loading];
    }

    return (
        <div>
            <h4>Tampilkan harga NAB/unit selama haha</h4>
            <form
                onSubmit={e => {
                    e.preventDefault();
                    setQuery(search);
                }}
            >
                <input type="text" onChange={e => setSearch(e.target.value)}/>
                <input type="submit" value="Bulan"/>
            </form>

            <p style={{fontSize: '9px'}}>
                {toString}
            </p>
        </div>
    )
};


export default MonthlyNAB;
