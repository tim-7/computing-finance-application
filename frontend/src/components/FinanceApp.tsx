import React, { useState, Fragment, useEffect } from 'react';
import { createStyles, makeStyles, useTheme, Theme } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Divider from '@material-ui/core/Divider';
import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from "@material-ui/pickers";
// import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

import MonthlyDeposit from "./MonthlyDeposit";
import MonthlyNAB from "./MonthlyNAB";
import NabGraph from "./NabGraph";
import LineGraph from "./LineGraph"
// import SavingGraph from "./SavingGraph"

const drawerWidth = 240;

const useStyles = makeStyles((theme: Theme) =>
  createStyles({
    root: {
      display: 'flex',
      flexDirection: 'row',
    },
    appBar: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
    },
    drawer: {
      width: drawerWidth,
      flexShrink: 0,
    },
    drawerPaper: {
      width: drawerWidth,
    },
    toolbar: theme.mixins.toolbar,
    content: {
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      padding: theme.spacing(3),
      flexDirection: 'row',
    },
    input: {
      margin: theme.spacing(1),
    },
    textField: {
      margin: theme.spacing(1),
      marginLeft: theme.spacing(3),
      marginRight: theme.spacing(1),
      width: 200,
    },
    graph: {
      
    }
  }),
);

const getMinDate = () => {
  let minDate = new Date();
  minDate.setMonth(minDate.getMonth() + 2);
  return minDate
};

const monthDiff = (dateFrom: Date, dateTo: Date) => {
  return dateTo.getMonth() - dateFrom.getMonth() + 
    (12 * (dateTo.getFullYear() - dateFrom.getFullYear()))
};

export default function FinanceApp() {
  const classes = useStyles();
  const [awal, setAwal] = useState(10000);
  const [target, setTarget] = useState(1000000);
  const [targetBulan, setTargetBulan] = useState(2);
  const [nabBulan, setNabBulan] = useState(12)
  const [nabQuery, setNabQuery] = useState(12)
  const [selectedDate, setSelectedDate] = useState<Date | null>(getMinDate());
  
  // return {
  //   value: selectedDate,
  //   onChange: handleDateChange,
  // };

  const [dataPoints, setDataPoints] = useState([
    {x: 0, y: 10},
    {x: 1, y: 100},
    {x: 2, y: 500},
])
  const [nabPoints, setNabPoints] = useState([
    {x: 0, y: 10},
    {x: 1, y: 100},
    {x: 2, y: 500},
  ])


  const getDataPoints = () => {
    console.log('monthly deposit')
    const urlFetch = fetch(
        'https://backend-seven.herokuapp.com/monthly-saving?start=' 
            + awal + '&bulan=' + (targetBulan + 1) + '&persen=' + 12
    )
    .then((response) => response.json())
    .then((data) => setDataPoints(data.map((value: number, index: number) => {
        return {x: index, y: value};
    })));
  }

  const getNABPoints = () => {
    console.log('nab => ' + nabBulan)
    const urlFetch = fetch(
        'http://backend-seven.herokuapp.com/monthly-nab?counter=' + nabBulan 
    )
    .then((response) => {
        // console.log(response.json())
        return response.json()
    })
    .then((data) => setNabPoints(data.map((value: number, index: number) => {
        return {x: index, y: value};
    })))
  }

  const handleAwalChange = (event: React.ChangeEvent) => {
    setAwal(parseInt((event.target as HTMLTextAreaElement).value));
  };

  const handleDateChange = (date: Date| null) => {
    setSelectedDate(date);
    getDataPoints()
  };

  const handleTargetChange = (event: React.ChangeEvent) => {
    setTarget(parseInt((event.target as HTMLTextAreaElement).value));
  };

  let handlers = [handleAwalChange, handleTargetChange]

  useEffect(() => {
    let tempDate = selectedDate || getMinDate();
    setTargetBulan(monthDiff(new Date(), tempDate));
  })

  useEffect(() => {
    document.title = targetBulan + " bulan"
  })

  return (
    <div className={classes.root}>
      <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap" />
      <CssBaseline />
      <AppBar position="fixed" className={classes.appBar}>
        <Toolbar>
          <Typography variant="h6" noWrap>
            Tabungan Cuan
          </Typography>
        </Toolbar>
      </AppBar>
      <Drawer
        className={classes.drawer}
        variant="permanent"
        classes={{
          paper: classes.drawerPaper,
        }}
        anchor="left"
      >
        <div className={classes.toolbar} />
        <Divider />
        {['Investasi Awal', 'Target'].map((text, index) => (
          <TextField
              id="outlined-number"
              label={text}
              type="number"
              className={classes.textField}
              InputLabelProps={{
                shrink: true,
              }}
              onChange={handlers[index]}
              margin="normal"
              variant="outlined"
            />
        ))}
        <MuiPickersUtilsProvider utils={DateFnsUtils}>
          <KeyboardDatePicker
            autoOk
            variant="inline"
            views={["year", "month"]}
            inputVariant="outlined"
            id="date-picker-inline"
            label="Target Waktu"
            minDate={getMinDate()}
            className={classes.textField}
            KeyboardButtonProps={{
              'aria-label': 'change date',
            }}
            value={selectedDate}
            onChange={handleDateChange}
          />
        </MuiPickersUtilsProvider>
        <div className={classes.textField}>
        <Typography paragraph>
            * Grafik Tabungan Impian akan terupdate setelah memilih tanggal
        </Typography>
        </div>
      </Drawer>
      <main className={classes.content}>
        <div className={classes.toolbar} />
        <div>
          <Typography paragraph>
            Raih Mimpimu dengan Memulai Simpanan Cuan!
          </Typography>
          <MonthlyDeposit awal={awal} total={target} targetBulan={targetBulan}/>
          <div className={classes.graph}>
            {/* <SavingGraph {...{
              start: awal,
              bulan: targetBulan,
              persen: 12,
            }
            }/> */}
            <Divider />
            <Typography paragraph>
              Raih Mimpimu dengan Memulai Tabungan Cuan!
            </Typography>
            <LineGraph {...{
              dataPoints: dataPoints,
              titleText: "Proyeksi Tabungan Impian",
            }}/>
          </div>
        </div>
        <Divider />
        <div>
        <Typography paragraph>
          Perhatikan Nilai Aktiva Bersih (NAB)!
        </Typography>
        </div>
        <Divider />
        <div>
          <input type="text" onChange={e => {
            console.log(e.target.value)
            if (e.target.value != ""){
              setNabBulan(parseInt(e.target.value))
              getNABPoints()
            }  
          }}/>
          <div className={classes.graph}>
          <LineGraph {...{
              dataPoints: nabPoints,
              titleText: "Harga NAB/bulan",
            }}/>
          </div>
        </div>
      </main>
    </div>
  );
}