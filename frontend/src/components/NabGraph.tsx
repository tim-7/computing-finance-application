import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types'
import CanvasJSReact from '../assets/canvasjs.react'
import { async } from 'q';

var CanvasJSChart = CanvasJSReact.CanvasJSChart;

const generateDataPoints = (noOfDps: number) => {
    var xVal = 1, yVal = 100;
    var dps = [];
    for(var i = 0; i < noOfDps; i++) {
      yVal = yVal +  Math.round(5 + Math.random() *(-5-5));
      dps.push({x: xVal,y: yVal});	
      xVal++;
    }
    return dps;
  }

const useForceUpdate = () => {
    let [value, setState] = useState(true);
    return () => setState(!value)
}

type NabProps = {
    bulan: number,
}

const NabGraph = ({bulan}: NabProps) => {
    const forceUpdate = useForceUpdate();
    const [dataPoints, setDataPoints] = useState([
        {x: 0, y: 10},
        {x: 1, y: 100},
        {x: 2, y: 500},
    ])

    useEffect(() => {
        const getDataPoints = async () => {
            console.log('nab => ' + bulan)
            const urlFetch = await fetch(
                'http://backend-seven.herokuapp.com/monthly-nab?counter=' + bulan 
            )
            .then((response) => {
                // console.log(response.json())
                return response.json()
            })
            .then((data) => setDataPoints(data.map((value: number, index: number) => {
                return {x: index, y: value};
            })))
        }
        getDataPoints()
    });

    const options = {
        theme: "light2", // "light1", "dark1", "dark2"
        animationEnabled: true,
        zoomEnabled: true,
        title: {
          text: "NAB/bulan"
        },
        axisY: {
          includeZero: false
        },
        axisX: {
            labelFormatter: function(e: any) {
                if (e.value === parseInt(e.value, 10)) {
                    let newDate = new Date()
                    newDate.setMonth(newDate.getMonth()+e.value);
                    let month = newDate.toLocaleString('default', { month: 'long' });
                    return month.slice(0, 3) + ' ' + newDate.getFullYear()
                }
                return "";
            }
        },
        data: [{
          type: "area",
          dataPoints: dataPoints
        }]
      }
    
    return (
        <div className="graph">
            <CanvasJSChart options = {options}></CanvasJSChart>
            {/* <button onClick={forceUpdate}>Refresh</button> */}
        </div>
    );
}

export default NabGraph;