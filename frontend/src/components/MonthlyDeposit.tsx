import React, { Props, useState, useEffect } from "react";

type DepositProps = {
  awal: number;
  total: number;
  targetBulan: number;
};

// const MonthlyDeposit: React.FC = (coba: props) => {
//   const greeting = 'Hello Function Component!';
//   return <h1>{greeting}</h1>;
// }

const MonthlyDeposit = ({ awal, total, targetBulan }: DepositProps) => {
  const [monthly, setMonthly] = useState(0);

  useEffect(() => {
    // Create an scoped async function in the hook
    async function anyNameFunction() {
      console.log('monthly deposit')
      const urlFetch = await fetch(
        'https://backend-seven.herokuapp.com/monthly-deposit?awal=' + awal + '&total=' + total + '&profit=0.005&targetbulan=' + targetBulan
        // 'localhost:3000/monthly-deposit?awal=' + awal + '&total=' + total + '&profit=0.005&targetbulan=' + targetBulan
      )
      .then((response) => response.json()).then((data) => setMonthly(data))
      // const json_data = await urlFetch.json();
      // setMonthly(json_data);

      // await loadContent();
    }
    // Execute the created function directly
    anyNameFunction();
  });

  return (
    <div>
      <h4>Setoran perbulan : </h4>
      <p>{monthly}</p>
    </div>
  );
};

export default MonthlyDeposit;
