import React from 'react';
import CanvasJSReact from '../assets/canvasjs.react'

var CanvasJSChart = CanvasJSReact.CanvasJSChart;

type LineGraphProps = {
    dataPoints: {x: number, y: number}[],
    titleText: string,
}

const LineGraph = ({ dataPoints, titleText }: LineGraphProps) => {
    const options = {
        theme: "light2", // "light1", "dark1", "dark2"
        animationEnabled: true,
        zoomEnabled: true,
        title: {
          text: titleText
        },
        axisY: {
          includeZero: false
        },
        axisX: {
            labelFormatter: function(e: any) {
                if (e.value === parseInt(e.value, 10)) {
                    let newDate = new Date()
                    newDate.setMonth(newDate.getMonth()+e.value);
                    let month = newDate.toLocaleString('default', { month: 'long' });
                    return month.slice(0, 3) + ' ' + newDate.getFullYear()
                }
                return "";
            }
        },
        data: [{
          type: "area",
          dataPoints: dataPoints
        }]
      }
    
    return (
        <CanvasJSChart options = {options}></CanvasJSChart>
    );
}

export default LineGraph;