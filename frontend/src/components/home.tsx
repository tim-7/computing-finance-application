import React from 'react';

import FinanceApp from './FinanceApp'

const Home: React.FC = () => {

  return (
    <FinanceApp />
  );
}

export default Home;
