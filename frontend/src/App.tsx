import React, { Props, useState, useEffect } from "react";
import CanvasJSReact from "./assets/canvasjs.react";
import "./App.css";
import MonthlyDeposit from "./components/MonthlyDeposit";
import MonthlyNAB from "./components/MonthlyNAB";

var CanvasJSChart = CanvasJSReact.CanvasJSChart;

// ref : https://canvasjs.com/react-charts/chart-zoom-pan/

const generateDataPoints = (noOfDps: number) => {
  var xVal = 1,
    yVal = 100;
  var dps = [];
  for (var i = 0; i < noOfDps; i++) {
    yVal = yVal + Math.round(5 + Math.random() * (-5 - 5));
    dps.push({ x: xVal, y: yVal });
    xVal++;
  }
  return dps;
};

const App: React.FC = () => {
  const [awal, setAwal] = useState(100000);
  const [total, setTotal] = useState(100000000);
  const [targetBulan, setTargetBulan] = useState(12);
  // const [isSubmited, setIssubmited] = useState(false)
  const [deposit, setDeposit] = useState(0)

  const options = {
    theme: "light2", // "light1", "dark1", "dark2"
    animationEnabled: true,
    zoomEnabled: true,
    title: {
      text: "Try Zooming and Panning"
    },
    axisY: {
      includeZero: false
    },
    data: [
      {
        type: "area",
        dataPoints: generateDataPoints(500)
      }
    ]
  };

  async function clickSubmit(event: React.MouseEvent) {
    event.preventDefault()
    fetch(
      'https://backend-seven.herokuapp.com/monthly-deposit?awal=' + awal + '&total=' + total + '&profit=0.005&targetbulan=' + targetBulan
    )
    .then((response) => response.json()).then((data) => setDeposit(data))
    // .then(data => console.log(data))
  }

  const handleAwalChange = (event: React.ChangeEvent) => {
    event.preventDefault();
    const awalValue = (event.target as HTMLTextAreaElement).value
    setAwal(parseInt(awalValue))

  };

  const handleWaktuChange = (event: React.ChangeEvent) => {
    event.preventDefault();
    const waktuValue = (event.target as HTMLTextAreaElement).value
    setTargetBulan(parseInt(waktuValue))

  };

  const handleTargetChange = (event: React.ChangeEvent) => {
    event.preventDefault();
    const targetValue = (event.target as HTMLTextAreaElement).value
    setTotal(parseInt(targetValue))

  };

  return (
    <div className="App">
      <header className="App-header">
        <form className="form">
          <div className="div-form">
            <label className="input-label">Investasi Awal</label>
            <input className="input-form" type="number" name="invest" value={awal} onChange={handleAwalChange}/>
          </div>
          <div className="div-form">
            <label className="input-label">Jangka Waktu (Bulan)</label>
            <input className="input-form" type="number" name="month" value={targetBulan} onChange={handleWaktuChange}/>
          </div>
          <div className="div-form">
            <label className="input-label">Target</label>
            <input className="input-form" type="number" name="target" value={total} onChange={handleTargetChange}/>
          </div>
          <input onClick={clickSubmit} type="submit" name="submit"/>
        </form>
        <p>{deposit}</p>
        {/* {isSubmited && renderItem()} */}
        {/* <MonthlyDeposit awal={awal} total={total} targetBulan={targetBulan}/>
        <MonthlyNAB priceNAB={[1058.271111111111]}/> */}
        <CanvasJSChart
          options={options}
          /* onRef={ref => this.chart = ref} */
        />
        
      </header>
    </div>
  );
};

export default App;
