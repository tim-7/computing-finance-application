module MonthlySaving where

{-
Fungsi ini berfokus ke tabungan yang menerima masukkan:
    1. keuntungan per tahun reksa dana (persen) dalam persen
    2. Tabungan Awal (start)
    3. Lama menyimpan (bulan)
Keluaran: Uang akhir setelah ditabung sebanyak x bulan
User story:
Toto ingin menabung uang sejumlah 500 juta karena ia menabung tidak ada perubahan
yang signifikan dalam tabungannya. Ia ingin menabung dalam bentuk reksa dana pada
reksa dana yang terpercaya dan ingin tahu setelah 2 tahun uangnya menjadi berapa.

Pemanggilan:
out = tabunganAkhir 500000000 6.234 24

[1000, 1011, 1022, 1033]

-}

monthlyPercentage persen = (persen/12)/100

geometricSequence start yearly = \bulan -> start * (1 + (monthlyPercentage yearly)) ** (bulan - 1)

listKenaikanPerBulan start bulan persen = map (geometricSequence start persen) [1..bulan]

{-
Dokumentasi Fungsi:
1.  Fungsi monthlyPercentage untuk mendapat persen bulanan dari persen reksa dana yang merupakan per tahun. 
    Input persen per tahun, output persen per bulan.

2.  Fungsi geometricSequence untuk mencari nilai deret geometric dari nilai awal dan ratio per bulan. Membutuhkan
    fungsi monthlyPercentage untuk mencari rasio dari hasil persen per bulan.

3.  listKenaikanPerBulan mengembalikan jumlah kenaikan tabungan per bulan dari tabungan.

>>> Menggunakan Functional Programming 
-}