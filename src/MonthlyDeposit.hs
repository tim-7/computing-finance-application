module MonthlyDeposit where

{-
hasilAkhir adalah fungsi yang menerima 
setoran awal(awal), 
setoran perbulan(perbulan), 
keuntungan reksadana dalam setahun(profit),
menabung reksadana berapa bulan(bulan)
Fungsi ini akan mengembalikan total uang yang akan didapat.

contoh:
Seseorang akan menggunakan reksadana dengan setoran awal 10000(10 ribu), 
orang tersebut akan membeli reksadana setiap bulannya 2000000(2 juta),
sekarang, reksadana perbulan memberi profit 0.551% perbulan,
orang tersebut akan membeli reksadana setiap bulan dalam waktu 6 bulan,
maka 

awal = 10000
perbulan = 2000000
profit = 0.00551
bulan = 6

result = hasilAkhir 10000 2000000 0.00551 6

-}


hasilAkhir awal perbulan profit 1 = (awal * (profit + 1))
hasilAkhir awal perbulan profit 2 = (awal * (profit + 1)) + perbulan
hasilAkhir awal perbulan profit bulan = ((hasilAkhir awal perbulan profit (bulan - 1) ) * (profit + 1)) + perbulan

result = hasilAkhir 10000 2000000 0.00551 6
ceiledResult = ceiling result

-- https://www.tokopedia.com/reksa-dana/dana-impian
tokopediaCeil a b c d = (ceiling ((hasilAkhir a b c d)/100 )) * 100

-- pangkat a b = a ^ b error jika menggunakan pangkat seperti ini(ambiguous)
pangkat a 0 = 1
pangkat a berapa = a * (pangkat a (berapa - 1))

pembagi profit 1 = 0
pembagi profit targetBulan = (pangkat (1 + profit) (targetBulan - 2)) + pembagi profit (targetBulan - 1)

countPerbulan awal total profit targetBulan = ( total - (awal * ( pangkat (profit + 1) (targetBulan - 1) )) ) / (pembagi profit targetBulan )

-- Fungsi ini digunakan jika sudah menerima profit terlebih dahulu dan menunggu data awal, total, targetBulan
changePosititionCountPerbulan profit awal total targetBulan = countPerbulan awal total profit targetBulan

profitValue = 0.005 -- Default value
curriedCountPerbulanWithFixedProfit = changePosititionCountPerbulan profitValue -- Currying, nilai dari profit sudah fix


{-
Fungsi ini menerima 
setoran awal(awal),
total uang yang ingin didapatkan(target),
perkiraan keuntungan perbulan(profit),
berapa lama akan menabung(targetBulan)
Fungsi ini akan mengembalikan berapa reksadana yang harus dibeli setiap bulannya
Contoh:
Seseorang membutuhkan uang 200000,
Dana awal orang tersebut adalah 10000
orang tersebut harus mendapatkan 200000 dalam waktu 6 bulan,
pada saat itu, reksadana memberikan keuntungan 10% setiap bulannya.

Cara menghitungnya adalah
hasil = ceiledPerbulan 10000 200000 0.1 6
hasil akan bernilai 30122 yang artinya setiap bulan harus membeli reksadana sebesar 30122

-}
-- Main Function
ceiledPerbulan awal total targetBulan = ceiling (curriedCountPerbulanWithFixedProfit awal total targetBulan)

ceiledPerbulanWaitProfit awal total targetBulan profit = ceiling (changePosititionCountPerbulan profit awal total targetBulan)