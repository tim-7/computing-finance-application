module Main where

import Control.Monad
import Happstack.Server (nullConf, simpleHTTP, toResponse, ok, dir, path, port, ServerPart, look, setHeader, toResponse, Response)
import MonthlyDeposit
import MonthlyNAB
import MonthlySaving
import Data.Typeable

import System.Environment(getEnv)



header = setHeader "Access-Control-Allow-Origin" "*"

monthlyDepositAPI :: ServerPart Response
-- API untuk menggunakan fungsi ceiledPerbulan, fungsi ceiledPerbulan ada di file MonthlyDeposit.hs
-- URL untuk test http://localhost:8000/monthly-deposit?awal=10000&total=200000&targetbulan=6
monthlyDepositAPI = 
    do awal <- look "awal"
       total <- look "total"
       targetBulan <- look "targetbulan"
       ok $ header $ toResponse $ show $ ceiledPerbulan (read awal) (read total) (read targetBulan)

-- API untuk menghitung deposit perbulan dengan parameter dana awal, dana target(total), 
-- target berapa bulan(targetBulan), dan profit keuntungan reksadana pada saat ini
-- URL untuk test http://localhost:8000/monthly-deposit-profit?awal=10000&total=200000&targetbulan=6&profit=0.005
monthlyDepositProfitAPI = 
    do awal <- look "awal"
       total <- look "total"
       targetBulan <- look "targetbulan"
       profit <- look "profit"
       ok $ header $ toResponse $ show $ ceiledPerbulanWaitProfit (read awal) (read total) (read targetBulan) (read profit)


monthlyNABAPI :: ServerPart Response
-- |testing = fungsi [1058.271111111111] (last([1058.271111111111])) 1
monthlyNABAPI = 
    do counter <- look "counter"
       ok $ header $ toResponse $ show $ curriedResultNAB (read counter)

monthlySavingAPI :: ServerPart Response
-- API untuk mencari kenaikan uang per bulan jika menyimpan sebuah reksa dana
-- URL untuk test http://localhost:8000/monthly-saving?start=100&bulan=12&persen=10
monthlySavingAPI = 
    do start <- look "start"
       bulan <- look "bulan"
       persen <- look "persen"
       ok $ header $ toResponse $ show $ listKenaikanPerBulan (read start) (read bulan) (read persen)

main :: IO ()
main = do
    port <- getEnv "PORT"
    let conf = nullConf { port = read port }
    simpleHTTP conf $ msum [dir "monthly-deposit" $ monthlyDepositAPI, dir "monthly-nab" $ monthlyNABAPI, dir "monthly-saving" monthlySavingAPI, dir "monthly-deposit-profit" $ monthlyDepositProfitAPI]
