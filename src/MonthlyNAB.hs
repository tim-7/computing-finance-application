module MonthlyNAB where

{-
Fungsi ini menghitung perkiraan harga NAB setiap bulannya selama setahun 
berdasarkan keuntungan per tahun yang dikeluarkan oleh pihak reksadana.
Sehingga bisa memperoleh data yang digunakan untuk grafik perkiraan harga NAB/unit setiap bulan selama 1 tahun

yearlyProfit adalah keuntungan per tahun
monthlyProfit didapatkan dengan cara membagi yearlyProfit dengan 12
currentNAB adalah harga NAB per unit pada saat ini

fungsi ini akan me-return sebuah list berisi double sebanyak 12 elemen (12 bulan)
fungsi list lastNAB counter :  
    -list ini awalnya berisi 1 saja, yaitu harga NAB/unit saat ini, lalu akan
    di append dengan perkiraan harga NAB/unit pada bulan berikutnya hingga 12 bulan
    -lastNAB ini akan mengambil elemen terakhir dari list karena harga NAB/unit bulan berikutnya
    ditentukan oleh harga NAB/unit bulan sebelumnya.
    -counter ini adalah sebagai counter saja, jika sudah 12 maka akan return list yang sudah berisi
    kemudian stop, dan tidak akan melanjutkan menghitung NAB lagi

Misal :
    monthlyProfit = 0.5
    Harga NAB/unit = 
        bulan 1 = 1058.271111111111 ; isi list = [1058.271111111111]
        bulan 2 = ((100 + 0.5)/100) * 1058.271111111111 = 1063.56246667; isi list = [1058.271111111111, 1063.56246667]
        bulan 3 = ((100 + 0.5)/100) * 1063.56246667 = 1068.880279; isi list = [1058.271111111111, 1063.56246667, 1068.880279]
        dst...
    
    Maka akan diperoleh hasil perkiraan NAB/unit bulanan selama 12 bulan (1 tahun)
    Dari sini kita bisa mendapatkan grafik perkiraan harga NAB/unit tiap bulannya selama 1 tahun.
-}

monthlyNAB yearlyProfit nab = ((100 + (yearlyProfit/12))/100) * nab

lastDariNAB listNAB = last(listNAB)

fungsi list lastNAB yearlyProfit 1  = list
fungsi list lastNAB yearlyProfit counter  = fungsi (list ++ [(monthlyNAB yearlyProfit (lastDariNAB list))]) (monthlyNAB yearlyProfit (lastDariNAB list)) yearlyProfit (counter - 1)

hasilNAB currentNAB = fungsi [currentNAB] (last([currentNAB]))

currentNAB = 1058.271111111111
yearlyProfit = 6.202
curriedResultNAB = hasilNAB currentNAB yearlyProfit


